# TradePRO
This repository consists of the front-end of the website and the backend API for the trading system. This repository also enables AutoPilot and trade order placement for the complete platform. The dependencies required to launch this project are : 

- Ruby v2.5.1 or greater.
- Rails v5.2.0 or greater.
- Redis Server v4.0.10 or greater.

**IMPORTANT : DON'T PUSH TO THE MASTER BRANCH EVER.**

# Setting up Locally

For the local setup of the application you'll need to clone the repository and follow the following commands.

```
$ git remote add origin git@bitbucket.org:akshitahluwalia/tradepro.git
$ cd TradePRO
$ bundle
$ rake db:create
$ rake db:migrate
$ rake db:seed
$ rails server
```
Don't forget to create an environment file ```.env``` for the configuration in the root directory of the project.

For the workers open a new terminal window and run the following command:
```
$ redis-server
```

This would run the server on **http://localhost:3000/**

## API Documentation & Wrapper Modules.

>Production Server for [TradePRO](https://www.tradepro.tech/).

## Setting up the Queues and Workers
> To be documented soon !..